<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Serv'Drone</title>
  <link rel="stylesheet" href="./stylesheets/style.css">
  <link rel="stylesheet" href="./stylesheets/support.css">
  <link rel="javascript" href="/css/master.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

  <?php include('assets/nav.php') ?>

  <main>


    <div class="container">

      <div class="faq">

        <form action="#" method="post">
          <label for="search_faq">Recherche par mots clés :</label>
          <input type="text" id="search_faq" name="search_faq" placeholder="Rechercher...">
        </form>

        <ul>
          <li><a href="#" id="deploy">Mon enfant n’est jamais revenu de l’école, comment le retrouver ?</a></li>
          <li><a href="#" id="deploy">Comment faire pour que mon drone arrête d’appeler la police ?</a></li>
          <li><a href="#">Puis-je pulvériser autre chose que des huiles essentielles ?</a></li>
          <li><a href="#">Serv’drone peut-il retrouver les pièges posés ?</a></li>
          <li><a href="#">Comment effacer les images de la caméra de surveillance ?</a></li>
        </ul>

      </div>
    </div>

  </main>

  <?php include('assets/value.php') ?>

  <?php include('assets/footer.php') ?>

</body>

</html>
