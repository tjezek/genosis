<nav>

<a href="index.php"><img class="logo" src="./images/logo.png" alt="logo"></a>

  <ul>
    <li><a href="about.php">A propos</a></li>
    <li><a href="index.php#product">Nos produits</a></li>
    <li><a href="support.php">Support</a></li>
    <li><a href="news.php">Actualités</a></li>
    <li><a href="#footer">Contact</a></li>
  </ul>

  <div class="icons">
    <a target="_blank" href="log.html"><img src="./images/icons/user.png" alt="Connexion"></a>
    <a href="cart.php"><img src="./images/icons/cart.png" alt="Panier"></a>
  </div>

</nav>
