<section id="footer">
  <div class="container">
    <div class="left">
      <form action="#" method="post">

        <label for="mail">E-mail</label>
        <input type="text" id="mail" name="mail" placeholder="Votre e-mail">

        <label for="object">Objet</label>
        <select id="object" name="object">
          <option value="sav">S.A.V</option>
          <option value="plainte">Réclamation</option>
          <option value="autre">Autre</option>
        </select>

        <label for="subject">Message</label>
        <textarea id="subject" name="subject" placeholder="Message" style="height:150px;"></textarea>

        <input type="submit" value="Envoyé">

      </form>
    </div>

    <div class="right">

      <div class="social">
        <div class="button">
          <a href="https://twitter.com/twitter?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-size="large" data-show-count="false">Suivre @ServDrone</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div class="button">
          <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
        </div>
        <a href="#"><img src="./images/icons/insta.png" alt="Instagram"></a>

      </div>

      <div class="list">

        <ul>
          <li><a href="#">Politique de confidentialité</a></li>
          <li><a href="#">Conditions d’utilisation</a></li>
          <li><a href="#">Mentions légales</a></li>
          <li><a href="#">Conditions générales de vente</a></li>
        </ul>

        <ul>
          <li><a href="support.php">Support</a></li>
          <li><a href="index.php#product">Nos produits</a></li>
          <li><a href="news.php">Actualités</a></li>
          <li><a href="about.php">A propos</a></li>
        </ul>

      </div>

      <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2"></script>
    </div>
  </div>
</section>
