<section id="value">
  <div class="icons">
    <img src="./images/icons/guarantee.png" alt="Garantie">
    <h3>Produits<br>garantis jusqu’à 5 ans !</h3>
  </div>
  <div class="icons">
    <img src="./images/icons/delivery.png" alt="Livraison">
    <h3>Livraison<br>express</h3>
  </div>
  <div class="icons">
    <img src="./images/icons/card.png" alt="Paiement">
    <h3>Paiement<br>sécurisé</h3>
  </div>
  <div class="icons">
    <img src="./images/icons/star.png" alt="Avis">
    <h3>Recommandé<br>par les clients</h3>
  </div>
</section>
