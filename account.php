<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Serv'Drone</title>
  <link rel="stylesheet" href="./stylesheets/style.css">
  <link rel="stylesheet" href="./stylesheets/account.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

  <?php include('assets/nav.php') ?>

  <main>

    <div class="container">

      <div class="under_container">

      <div class="left">
        <h2>Mes commandes</h2>
        <hr>

        <table>
          <thead>
            <tr>
              <th scope="col">N° de commande</th>
              <th scope="col">Produit</th>
              <th scope="col">Livraison à</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">20190330156</th>
              <td>Le modèle Class 1 - 10 fonctions de base</td>
              <td>Adresse par défaut</td>
              <td>299€ TTC</td>
            </tr>
            <tr>
              <th scope="row">20190330265</th>
              <td>Le modèle Plus - 15 fonctions</td>
              <td>"Chez polo"</td>
              <td>399€ TTC</td>
            </tr>
          </tbody>
        </table>

        <div class="bloc">

          <h2>Mon S.A.V</h2>
          <hr>

        </div>
      </div>

      <div class="right">

          <h2>Bonjour, Timothée</h2>
          <hr>
          <p><a href="#">Compléter votre profil</a></p>

        <div class="bloc">
          <h2>Mon identité</h2>
          <hr>
          <p>09/03/1997</p>
          <p>Identifiant : 69596959</p>
          <p>Mail : mail@mail.com</p>
          <button type="button" name="button">En savoir plus</button>
        </div>

        <div class="bloc">
          <h2>Mes adresses</h2>
          <hr>
          <p><strong>Adresse par défaut :</strong></p>
          <p>Robet Emit</p>
          <p>50 av de Paris</p>
          <p>59000, Lille</p>
          <button type="button" name="button">En savoir plus</button>
        </div>

        <div class="bloc">
          <h2>Mes moyens de paiement</h2>
          <hr>
          <p><strong>Moyen de paiement par défaut :</strong></p>
          <p>Paypal</p>
          <button type="button" name="button">En savoir plus</button>
        </div>

      </div>
      </div>

    </div>

  </main>

    <?php include('assets/value.php') ?>

    <?php include('assets/footer.php') ?>

</body>

</html>
