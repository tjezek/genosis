<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Serv'Drone</title>
  <link rel="stylesheet" href="./stylesheets/style.css">
  <link rel="stylesheet" href="./stylesheets/about.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

  <?php include('assets/nav.php') ?>


  <main>
      <div class="container">

      <div class="align">

          <div class="presentation">
              <h2>Présentation</h2>
              <p>Serv’drone est une start-up française créée en 2016 par trois amis d’enfance
                  passionnés par l’IoT (Les objets connectés). Leur objectif principal est d’allier
                  technologie et service à la personne. Serv’drone commercialise des drones nouvelle
                  génération aux fonctions révolutionnaires comme escorter vos enfants sur le chemin
                  de l’école, promener vos animaux domestiques, récupérer vos colis à la poste,
                  retrouver vos clés égarées. Bref la technologie au service du quotidien. </p>
          </div>
          <div class="description">
              <div class="box">
                  <img src="./images/photo_tom.png"   alt="photo de Tom">
                  <div class="text">
                      <h3>Tom</h3>
                    <p>Après avoir perdu des millions d’amis sur Myspace, Tom trouveune idée lumineuse
                    en proposant à deux camarades de créer Serv’Drone.<br>
                    <br>
                    Lorem ipsum dolor sit amet et delectus accommodare hisconsul copiosae legendos
                    at vix ad putent delectus delicata usu. Viditdissentiet eos cu eum an brute copiosae
                    hendrerit. Eos erant dolorum an. Per facer affertut. Mei iisque mentitum moderatius
                    cu. Sit munere facilis accusam eu dicat falli consulatuat vis.Te facilisis
                    mnesarchum   qui   posse   omnium   mediocritatem   est   cu.   </p>
                </div>

              </div>
              <div class="box">
                    <img src="./images/photo_jo.png"  alt="photo de Jonathan">
                    <div class="text">
                        <h3>Jonathan</h3>
                      <p>Après avoir perdu des millions d’amis sur Myspace, Tom trouveune idée lumineuse
                      en proposant à deux camarades de créer Serv’Drone.<br>
                      <br>
                      Lorem ipsum dolor sit amet et delectus accommodare hisconsul copiosae legendos
                      at vix ad putent delectus delicata usu. Viditdissentiet eos cu eum an brute copiosae
                      hendrerit. Eos erant dolorum an. Per facer affertut. Mei iisque mentitum moderatius
                      cu. Sit munere facilis accusam eu dicat falli consulatuat vis.Te facilisis
                      mnesarchum   qui   posse   omnium   mediocritatem   est   cu.   </p>
                    </div>
              </div>
              <div class="box">
                    <img src="./images/photo_arturo.png"  alt="photo de Arturo">
                    <div class="text">
                        <h3>Arturo</h3>
                      <p>Après avoir perdu des millions d’amis sur Myspace, Tom trouveune idée lumineuse
                      en proposant à deux camarades de créer Serv’Drone.</br>
                      <br>
                      Lorem ipsum dolor sit amet et delectus accommodare hisconsul copiosae legendos
                      at vix ad putent delectus delicata usu. Viditdissentiet eos cu eum an brute copiosae
                      hendrerit. Eos erant dolorum an. Per facer affertut. Mei iisque mentitum moderatius
                      cu. Sit munere facilis accusam eu dicat falli consulatuat vis.Te facilisis
                      mnesarchum   qui   posse   omnium   mediocritatem   est   cu.   </p>
                    </div>
              </div>

            </div>
          </div>
      </div>
  </main>

  <?php include('assets/value.php') ?>

  <?php include('assets/footer.php') ?>

</body>

</html>
