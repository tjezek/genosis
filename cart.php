<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Serv'Drone</title>
  <link rel="stylesheet" href="./stylesheets/style.css">
  <link rel="stylesheet" href="./stylesheets/progress.css">
  <link rel="stylesheet" href="./stylesheets/cart.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

  <?php include('assets/nav.php') ?>

  <main>

    <div class="container">
<!-- is-complete -->
<!-- is-active -->
        <ol class="progress-bar">
          <li class="is-active"><span>Mon panier</span></li>
          <li><span>Identification</span></li>
          <li><span>Livraison</span></li>
          <li><span>Paiement</span></li>
          <li><span>Validation</span></li>
        </ol>

        <div class="under_container">

        <div class="left">
          <h2>Mon Panier</h2>
          <hr>

          <table>
            <thead>
              <tr>
                <th scope="col">Produit</th>
                <th scope="col">Référence</th>
                <th scope="col">Quantité</th>
                <th scope="col">Prix</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">Le modèle Class 1 - 10 fonctions de base</th>
                <td>SERV001</td>
                <td>1</td>
                <td>299€</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="right">
          <h2>Récapitulatif</h2>
          <hr>
          <p>Quantité : 1</p>
          <p>Sous total : 299€</p>

          <button type="button" name="button">Valider</button>

        </div>

      </div>

    </div>

  </main>

  <?php include('assets/value.php') ?>

  <?php include('assets/footer.php') ?>

</body>

</html>
