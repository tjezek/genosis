<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Serv'Drone</title>
  <link rel="stylesheet" href="./stylesheets/style.css">
  <link rel="stylesheet" href="./stylesheets/news.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

  <?php include('assets/nav.php') ?>

  <main>

    <div class="container">

        <div class="box">

          <div class="left">

            <h2>Événements</h2>

            <img src="./images/drones_expo.jpg" alt="Affiche salon du drone">
            <p>La première édition du Drone Paris Région Expo aura lieu les 27, 28 et 29
              septembre 2018 sur le centre d’essais en vol de Brétigny sur Orge. Ce salon
              consacré au marché des drones civils et militaires rassemblera une centaine d’exposants pendant 2 jours dédiés aux professionnels
              et une 3ème journée ouverte au grand public.</p>
          </div>

          <div class="right">

            <h2>Presse</h2>

            <div class="publish">
              <a href="#"><img src="./images/presse_1.jpg" alt="article de presse 1"></a>
              <span>Article 1</span>
              <p>Lorem ipsum dolor sit amet et delectus accommodare his consul copiosae
                legendos at vix ad putent delectus delicata usu. Vidit dissentiet eos
                cu eum an brute copiosae hendrerit.</p>
            </div>

            <div class="publish">
              <a href="#"><img src="./images/presse_2.jpg" alt="article de presse 2"></a>
              <span>Article 2</span>
              <p>Lorem ipsum dolor sit amet et delectus accommodare his consul copiosae
                legendos at vix ad putent delectus delicata usu. Vidit dissentiet eos
                cu eum an brute copiosae hendrerit.</p>
            </div>
          </div>
        </div>

    </div>

  </main>

  <?php include('assets/value.php') ?>

  <?php include('assets/footer.php') ?>

</body>

</html>
