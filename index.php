<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Serv'Drone</title>
  <link rel="stylesheet" href="./stylesheets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

  <?php include('assets/nav.php') ?>

  <header>

    <div class="text_box">
      <h1>La technologie <br>au service <br>du quotidien</h1>
    </div>

  </header>

  <main>

    <div class="partner">
      <h2>Nos partenaires</h2>
      <div class="partner_box">
        <img src="./images/ctdrone.png" alt="CT Drone">
        <span class="border"></span>
        <img src="./images/easydrone.png" alt="Easy Drone">
        <span class="border"></span>
        <img src="./images/dronemoilamain.png" alt="Drone Moi La Main">
      </div>
    </div>

    <section id="present">
      <h1>Qu'est-ce que Serv'Drone ?</h1>
      <div class="container">
        <div class="envelope">
          <h3><img class="icon_title" src="./images/icons/camera.png" alt="camera">Caméra de surveillance</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nunc arcu, aliquam ultrices gravida quis, congue in ex. Pellentesque aliquet cursus ante in elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas.</p>

          <h3><img class="icon_title" src="./images/icons/box.png" alt="colis">Récupérer vos colis à la poste</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscng elit. Nullam nunc arcu, aliquam ultrices gravida quis, congue in ex. Pellentesque aliquet cursus ante in elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas.</p>

          <h3><img class="icon_title" src="./images/icons/key.png" alt="clé">Retrouver vos clés égarées</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nunc arcu, aliquam ultrices gravida quis, congue in ex. Pellentesque aliquet cursus ante in elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas.</p>
        </div>
        <img src="./images/drone.png" alt="Drone">
        <div class="envelope">
          <h3><img class="icon_title" src="./images/icons/dog.png" alt="chien">Promène vos animaux</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nunc arcu, aliquam ultrices gravida quis, congue in ex. Pellentesque aliquet cursus ante in elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas.</p>

          <h3><img class="icon_title" src="./images/icons/book.png" alt="livre">Raconte des histoires à vos enfants</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nunc arcu, aliquam ultrices gravida quis, congue in ex. Pellentesque aliquet cursus ante in elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas.</p>

          <h3><img class="icon_title" src="./images/icons/car.png" alt="auto">Stationnement de votre véhicule</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nunc arcu, aliquam ultrices gravida quis, congue in ex. Pellentesque aliquet cursus ante in elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas.</p>
        </div>
      </div>
    </section>

    <div class="appli">
      <div class="appli_box">
        <a href="#"><img src="./images/icons/playstore.png" alt=""></a>
        <span>Nos drones sont dirigés via notre application gratuite<br>Pensez à la télécharger !</span>
        <a href="#"><img src="./images/icons/applestore.png" alt=""></a>
      </div>
    </div>

    <section id="product">
      <h2>Le modèle Class 1 - 10 fonctions de base</h2>
      <div class="box">
        <div class="photo pd1"></div>
        <div class="text">
          <ul>
            <li>Détecteur de mouvement</li>
            <li>Caméra de surveillance</li>
            <li>Assistance dans le stationnement de votre véhicule</li>
            <li>Escorter vos enfants sur le chemin de l’école</li>
            <li>Promener vos animaux domestiques</li>
            <li>Récupérer vos colis à la poste</li>
            <li>Retrouver vos clés égarées</li>
            <li>Raconte des histoires à vos enfants</li>
            <li>Vous assiste dans la préparation des repas</li>
            <li>Visiophonie</li>
          </ul>

          <p>GARANTIE 2 ANS</p>
          <span>299 € <a href="#"><img src="./images/icons/buy.png" alt="acheter"></a></span>
        </div>
      </div>

      <h2>Le modèle Plus - 15 fonctions</h2>
      <div class="box">
        <div class="photo pd2"></div>
        <div class="text">
          <p>Le modèle Class 1 avec en plus :</p>
          <ul>
            <li>Repassage de vos vêtements</li>
            <li>Dépoussiérage de vos meubles</li>
            <li>Navette Domicile / Pressing</li>
            <li>Nettoyage de vos animaux</li>
            <li>Pulvérisateur automatique d’huiles essentielles</li>
          </ul>

          <p>GARANTIE 2 ANS</p>
          <span>399 € <a href="#"><img src="./images/icons/buy.png" alt="acheter"></a></span>
        </div>
      </div>

      <h2>Le modèle Deluxe - 20 fonctions</h2>
      <div class="box">
        <div class="photo pd3"></div>
        <div class="text">
          <p>Le modèle Plus avec en plus :</p>
          <ul>
            <li>Surveillance de votre domicile intérieur et extérieur</li>
            <li>Mise en relation avec les services de Police</li>
            <li>Neutralisation électrique des assaillants</li>
            <li>Pose des pièges de manière aléatoire</li>
            <li>Dissimulation des preuves</li>
          </ul>

          <p>GARANTIE 5 ANS</p>
          <span>499 € <a href="#"><img src="./images/icons/buy.png" alt="acheter"></a></span>
        </div>
      </div>
    </section>

    </main>


  <?php include('assets/value.php') ?>

  <?php include('assets/footer.php') ?>

</body>

</html>
